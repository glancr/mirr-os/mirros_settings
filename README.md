# mirr.OS one Settings App

Vue.js SPA to configure a [mirr.OS API](https://gitlab.com/glancr/mirros_api/) installation.

## Project setup

```shell
# installs dependencies to node_modules
yarn install
```

```shell
yarn serve          # Compiles and hot-reloads for development

yarn build          # Compiles and minifies for production

yarn lint           # Lints and fixes files

yarn test:unit      # Runs unit and snapshot tests
```
