const path = require("path");

// https://vue-styleguidist.github.io for documentation
module.exports = {
  title: "Default Style Guide",
  require: [path.join(__dirname, "src/assets/sass/main.scss")],
  components: "src/components/forms/[A-Z]*.vue",
  ignore: ["**/*.spec.vue", "src/components/forms/FormControlToggle.vue"],
  showUsage: true,
};
