/**
 * Patches gridstack.js prototypes to enable resizing widget instances in both directions. See https://github.com/gridstack/gridstack.js#extend-library
 */
import { GridStack } from "gridstack";
GridStack.prototype.resize = function (
  el,
  width,
  height,
  resizeBothDirections
) {
  this._updateElement(el, function (el, node) {
    width = width !== null && width !== undefined ? width : node.width;
    height = height !== null && height !== undefined ? height : node.height;

    this.engine.moveNode(
      node,
      node.x,
      node.y,
      width,
      height,
      false,
      resizeBothDirections
    );
  });
};

GridStack.Engine.prototype.moveNode = function (
  node,
  x,
  y,
  width,
  height,
  noPack,
  resizeBothDirections
) {
  if (node.locked) {
    return null;
  }
  if (typeof x !== "number") {
    x = node.x;
  }
  if (typeof y !== "number") {
    y = node.y;
  }
  if (typeof width !== "number") {
    width = node.width;
  }
  if (typeof height !== "number") {
    height = node.height;
  }

  // constrain the passed in values and check if we're still changing our node
  let resizing = node.width !== width || node.height !== height;
  let nn = {
    x,
    y,
    width,
    height,
    maxWidth: node.maxWidth,
    maxHeight: node.maxHeight,
    minWidth: node.minWidth,
    minHeight: node.minHeight,
  };
  nn = this.prepareNode(nn, resizeBothDirections ? false : resizing);
  if (
    node.x === nn.x &&
    node.y === nn.y &&
    node.width === nn.width &&
    node.height === nn.height
  ) {
    return null;
  }

  node._dirty = true;

  node.x = node._lastTriedX = nn.x;
  node.y = node._lastTriedY = nn.y;
  node.width = node._lastTriedWidth = nn.width;
  node.height = node._lastTriedHeight = nn.height;

  this._fixCollisions(node);
  if (!noPack) {
    this._packNodes();
    this._notify();
  }
  return node;
};
