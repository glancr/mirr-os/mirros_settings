import { shallowMount, createLocalVue } from "@vue/test-utils";
import { validate } from "@/directives/validate";
import PersonalSettingsBlock from "@/components/config/PersonalSettingsBlock.vue";
import VueTranslate from "vue-translate-plugin";

describe("PersonalSettingsBlock.vue", () => {
  const localVue = createLocalVue();
  localVue.use(VueTranslate);
  localVue.directive("validate", validate);

  const wrapper = shallowMount(PersonalSettingsBlock, {
    propsData: {
      personal_name: { value: "" },
      personal_email: { value: "" },
    },
    localVue,
  });

  it("should only accept valid e-mails", () => {
    wrapper.find("#personal_email").setValue("invalidmail");
    expect(wrapper.find("#personal_email").attributes(":invalid"));
  });

  it("should emit a formupdate event on change", () => {
    const name = wrapper.find("#personal_name");
    name.setValue("A name");
    name.trigger("change");

    const email = wrapper.find("#personal_email");
    email.setValue("mail@example.com");
    email.trigger("change");

    const formUpdateEvents = wrapper.emitted().formupdate;

    expect(formUpdateEvents.length).toBe(2);
    expect(formUpdateEvents[0][0].target.value).toBe("A name");
    expect(formUpdateEvents[1][0].target.value).toBe("mail@example.com");
  });
});
